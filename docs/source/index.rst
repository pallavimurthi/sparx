.. sparx documentation master file, created by
   sphinx-quickstart on Wed Jun 28 16:20:51 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Sparx's documentation!
=======================

.. image:: nstatic/logo.png
    :target: http://sparx.readthedocs.org

Sparx is an exclusive data preprocessing library which involves in transforming raw data into an machine understandable format. We at CleverInsight Lab took the initiative to build a better automated data preprocessing library and here it is.

.. toctree::
   :maxdepth: 2

   help
   installation
   usage
   authors
   contributing
   history
   


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
