:orphan:

=======
Authors
=======

Development Lead
----------------

* `Bastin Robins <https://github.com/bastinrobin>`__ <robin@cleverinsight.co>
* `Vandana Bhagat <https://github.com/vandana-11>`__ <vandana.bhagat@cleverinsight.co>
* `Pallavi Murthi <https://github.com/pallavimurthi>`__ <pallavi.murthy@cleverinsight.co>

Contributors
-------------

* `Kothandaraman Sridharan <https://github.com/ksmadras2000>`__ <sri@cleverinsight.co>
* `Anusha Sridharan <https://github.com/anushas007>`__ <anu.sridharan@gmail.com>
* `Aruna Sridharan <https://github.com/arunasridharan>`__ <aruna.sridharan@gmail.com>
